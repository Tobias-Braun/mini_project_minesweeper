package application;

import javafx.scene.control.Button;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;


public class GameField extends Button{

	public GameField(boolean isBomb, double BWIDTH, double BHEIGHT){
		this.setText("  ");
		this.isBomb = isBomb;
		this.setMinWidth(BWIDTH);
		this.setMaxWidth(BWIDTH);
		this.setMinHeight(BHEIGHT);
		this.setMaxHeight(BHEIGHT);
	}
	private boolean activated = false;
	private boolean bombmarked = false;
	private boolean isBomb;
	private int value;
	
	public boolean isHandeledCorrectly(){
		if (activated == true || bombmarked == true && isBomb == true) return true;
		return false;
	}

	public boolean isBomb(){
		return isBomb;
	}
	public int getValue(){
		return value;
	}
	public void incValue(){
		value++;
	}
	public boolean notActivated(){
		return !activated;
	}
	public boolean handle(MouseEvent e){
		if (e.getButton() == MouseButton.SECONDARY){
			if (bombmarked) {
				bombmarked = false;
				this.setText("?");
				this.setStyle("-fx-background-color: #d8d8d8;");
			}
			else {
				bombmarked = true;
				this.setText("F");
				this.setStyle("-fx-background-color: #e2e2e2;-fx-border-color: #ff1111;");
			}
		}
		else if (e.getButton() == MouseButton.PRIMARY){
			activated = true;
			if (isBomb) {
				this.setText("B");
				this.setStyle("-fx-background-color: #ff0000;-fx-text-fill: #ffffff");
				return true;
			}
			else if (value != 0) {
				this.setText(Integer.valueOf(value).toString());
				switch (value){
				case 1: this.setStyle("-fx-text-fill: #0000ff;-fx-background-color: #e2e2e2;-fx-font-size: 12.1;-fx-font-weight: bold;");break;
				case 2: this.setStyle("-fx-text-fill: #008800;-fx-background-color: #e2e2e2;-fx-font-size: 11.5;-fx-font-weight: bold;");break;
				case 3: this.setStyle("-fx-text-fill: #ff0000;-fx-background-color: #e2e2e2;-fx-font-size: 11.5;-fx-font-weight: bold;");break;
				case 4: this.setStyle("-fx-text-fill: #880088;-fx-background-color: #e2e2e2;-fx-font-size: 11.5;-fx-font-weight: bold;");break;
				case 5: this.setStyle("-fx-text-fill: #800000;-fx-background-color: #e2e2e2;-fx-font-size: 11.5;-fx-font-weight: bold;");break;
				case 6: this.setStyle("-fx-text-fill: #00e5ee;-fx-background-color: #e2e2e2;-fx-font-size: 11.5;-fx-font-weight: bold;");break;
				case 7: this.setStyle("-fx-text-fill: #000000;-fx-background-color: #e2e2e2;-fx-font-size: 11.5;-fx-font-weight: bold;");break;
				case 8: this.setStyle("-fx-text-fill: #343434;-fx-background-color: #e2e2e2;-fx-font-size: 11.5;-fx-font-weight: bold;");break;
				}
			} else this.setStyle("-fx-background-color: #E2E2E2;");

		}
		return false;
	}


}
