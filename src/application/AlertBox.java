package application;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlertBox {
	int[] settings = {1, 20, 15};

	public int[] display(String message){
		Label gamestatus = new Label(message);
		Label n_bombs = new Label("Propability of Bombs:");
		Slider bombslider = new Slider();
		SimpleIntegerProperty bombs = new SimpleIntegerProperty(settings[1]);
		bombs.bind(bombslider.valueProperty());
		SimpleIntegerProperty fields = new SimpleIntegerProperty(settings[2]);
		bombslider.setMin(0);
		bombslider.setMax(100);
		bombslider.setValue(settings[1]);
		bombslider.setShowTickLabels(true);
		bombslider.setShowTickMarks(true);
		bombslider.setMajorTickUnit(10);
		bombslider.setMinorTickCount(10);
		Label n_fields = new Label("Fieldsize:");
		Slider fieldslider = new Slider();
		fields.bind(fieldslider.valueProperty());
		fieldslider.setMin(5);
		fieldslider.setMax(25);
		fieldslider.setValue(settings[2]);
		fieldslider.setShowTickLabels(true);
		fieldslider.setShowTickMarks(true);
		fieldslider.setMajorTickUnit(5);
		fieldslider.setMinorTickCount(4);
		Stage window = new Stage();
		VBox layout = new VBox();
		Button yesButton = new Button("GO");
		yesButton.setOnAction(e ->{
			window.close();
		});
		Button noButton = new Button("Exit");
		noButton.setOnAction(e -> {
			settings[0] = 0;
			window.close();
		});
		layout.getChildren().addAll(gamestatus, n_bombs, bombslider, n_fields, fieldslider, yesButton, noButton);
		Scene scene = new Scene(layout, 300, 200);
		scene.getStylesheets().add(getClass().getResource("settings.css").toExternalForm());
		window.setScene(scene);
		window.initModality(Modality.APPLICATION_MODAL);
		window.setTitle("Settings");
		window.showAndWait();
		settings[1] = bombs.getValue();
		settings[2] = fields.getValue();
		return settings;
	}
}
