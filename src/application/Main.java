package application;

import java.util.Random;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.media.AudioClip;


public class Main extends Application {

	//AudioClip s_hover = new AudioClip(this.getClass().getResource("sound_hover.wav").toString());
	AudioClip s_click = new AudioClip(this.getClass().getResource("sound_click.wav").toString());
	boolean lost = false;
	Stage window;
	Scene scene;
	GridPane gridContainer;
	static private AlertBox Box = new AlertBox();
	static private double BWIDTH = 25;
	static private int BPROB = 40;
	static private double BHEIGHT = 25;
	static private int SIZE = 20;
	static private final String WINMESSAGE = "Congratulations! You won the game!";
	static private final String LOSEMESSAGE = "You lost - you are bad as hell!";
	private GameField[][] grid;	
	private void startNewRound(String message){
		try {
			// create Grid with Set Values and Bomb booleans:
			int[] settings = Box.display(message);
			SIZE = settings[2];
			BPROB = settings[1];
			if (settings[0] == 0) Platform.exit();
			BWIDTH = 25;
			BHEIGHT = 25;
			gridContainer.getChildren().clear();
			initialise(gridContainer, window);
			if (window != null) window.show();

		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private boolean checkwon(){
		for (GameField[] row : grid){
			for(GameField cell : row){
				if (!cell.isHandeledCorrectly()) return false;
			}
		}
		return true;
	}

	private void handle(MouseEvent e, int i, int j){
		if (i >= 0 && j >= 0 && j < SIZE && i < SIZE && grid[i][j].notActivated()) {
			s_click.play();
			lost = grid[i][j].handle(e);
			if (lost){
				startNewRound(LOSEMESSAGE);
				return;
			}
			
			if(checkwon()){
				startNewRound(WINMESSAGE);
				return;
			}
			if (e.getButton() == MouseButton.PRIMARY){
				
				if (grid[i][j].getValue() == 0){
					if (i > 0){
						handle(e, i-1, j);
						if (j > 0 ) handle(e, i-1, j-1);
						if (j < SIZE-1 ) handle(e, i-1, j+1);
					}
					if (i < SIZE-1){
						handle(e, i+1, j);
						if (j > 0 ) handle(e, i+1, j-1);
						if (j < SIZE-1 ) handle(e, i+1, j+1);
					}
					if (j > 0 ) handle(e, i, j-1);
					if (j < SIZE-1 ) handle(e, i, j+1);
				}
			}
		}

	}

	private void initialise(GridPane gridContainer, Stage window){
		grid = new GameField[SIZE][SIZE];
		gridContainer.setMaxWidth(SIZE*BWIDTH);
		gridContainer.setMaxHeight(SIZE*BHEIGHT);
		Random r = new Random();
		for (int i = 0; i < SIZE; i++){
			for (int j = 0; j < SIZE; j++){
				if (r.nextInt(1000) < BPROB*10)   grid[i][j] = new GameField(true, BWIDTH, BHEIGHT);
				else  grid[i][j] = new GameField(false, BWIDTH, BHEIGHT);
				final int ival = i;
				final int jval = j;
				grid[i][j].setOnMouseClicked(e -> handle(e, new Integer(ival), new Integer(jval)));
				gridContainer.add(grid[i][j], i, j);
			}
		}
		for (int i = 0;i<grid.length; i++){
			for (int j = 0; j < grid[0].length; j++){

				//check all surrounding GameFields:

				if (i > 0){
					if (grid[i-1][j].isBomb()) grid[i][j].incValue();
					if (j >0) if (grid[i-1][j-1].isBomb()) grid[i][j].incValue();
					if (j < SIZE-1) if (grid[i-1][j+1].isBomb()) grid[i][j].incValue();
				}
				if (i < SIZE-1){
					if (grid[i+1][j].isBomb()) grid[i][j].incValue();
					if (j > 0) if (grid[i+1][j-1].isBomb()) grid[i][j].incValue();
					if (j < SIZE-1) if (grid[i+1][j+1].isBomb()) grid[i][j].incValue();
				}
				if (j > 0) if (grid[i][j-1].isBomb()) grid[i][j].incValue();
				if (j < SIZE-1) if (grid[i][j+1].isBomb()) grid[i][j].incValue();
			}
		}
		window.setWidth(SIZE*(BWIDTH+0.2)-0.5);
		window.setHeight(SIZE*BHEIGHT+BHEIGHT);
	}

	public void stop(){
	}

	@Override
	public void start(Stage stage) {
		window = stage;
		window.setResizable(false);
		gridContainer = new GridPane();
		initialise(gridContainer, window);
		scene = new Scene(gridContainer,gridContainer.getWidth(), gridContainer.getHeight());
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		window.setScene(scene);
		window.getIcons().add(new Image(getClass().getResource("icon.png").toString()));
		window.setTitle("MinesweeperFX");
		/*window.widthProperty().addListener((obs, oldval, newval) -> {
			BWIDTH = newval.doubleValue()/SIZE-1;
			for (GameField[] row : grid){
				for (GameField cell : row){
					cell.setMinWidth(BWIDTH);
					cell.setMaxWidth(BWIDTH);
				}
			}
		});
		window.heightProperty().addListener((obs, oldval, newval) ->{
			BHEIGHT = newval.doubleValue()/SIZE-(1);
			for (GameField[] row : grid){
				for (GameField cell : row){
					cell.setMinHeight(BHEIGHT);
					cell.setMaxHeight(BHEIGHT);
				}
			}
		});*/
		window.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
